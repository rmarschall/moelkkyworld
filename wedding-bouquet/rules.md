# Wedding Bouquet Mölkky a.k.a. Back Sided Mölkky

## Rules

This version of Mölkky follows the standard rules except that players must turn their backs toward the Mölkky. They can only throw over their head (like a wedding bouquet), under their legs, or backwards on the side.

As many non-hits are expected, the `3 times miss` rule is suspended.

This version of Mölkky is very difficult and shall remain only for the most expert players. Otherwise it's quite dull to play.


## Origin
It was first payed in front of the Pavillon Henri Chrétien (PHC) of the Observatoire de la Côte d’Azur (OCA) on 06.03.2024.
