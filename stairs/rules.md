# Stairs Mölkky

## Rules
This version of Mölkky follows the standard rules except that the Mölkky is set on the steps of stairs. The players play from below. Numbers may only be hit when they are either 1) still on the stairs or 2) if the wood stick first hits the stairs and then rolls down to hit a number on the 'ground floor'.
The game ends if a team reaches 50 points or if all numbers have reached the bottom of the stairs.

Alternatively, when all numbers have reached the bottom of the stairs they can be directly taken down.


## Origin
It was first payed in front of the Pavillon Henri Chrétien (PHC) of the Observatoire de la Côte d’Azur (OCA) on 16.02.2024.
