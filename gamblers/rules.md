# Gamblers Mölkky

## Rules
This version of Mölkky follows the standard rules except that the Mölkky a team can bet on an outcome to change the other teams score.
A team can bet that it will hit a certain pin or a certain number of pins. If they manage to throw the predicted combination they gain those point but also the other team loses those points. If the team misses the prediction they lose the number of points thrown.


## Origin
It was first payed in front of the Pavillon Henri Chrétien (PHC) of the Observatoire de la Côte d’Azur (OCA) on 04.03.2024.
