# Extreme Mölkky

## Rules

This version of Mölkky follows the standard rules except that the Mölkky is set up at the bottom of a set of stairs and the players play down from the top of the stairs. From how far up the stairs the game is played will determine how extreme the game becomes.

Optionally you may add the rule that once a stick has passed a certain distance (e.g., 6 m) they no longer count as being in the game.


## Origin
It was first payed in front of the Pavillon Henri Chrétien (PHC) of the Observatoire de la Côte d’Azur (OCA) on 15.02.2024.
