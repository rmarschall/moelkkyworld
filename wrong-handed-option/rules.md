# Wrong-Handed Option (WHO) Mölkky

## Rules

This version of Mölkky follows the standard rules except that players cannot use their dominant hand for throws.
So a right-handed player throw with her/his left hand, and vice versa.
This is clearly the wrong-handed option (WHO), but not recommended by 
[WHO](https://www.who.int/).

As many non-hit are expected, the follwing rule is added: three misses bring you back to 
zero or 25 (if the team was above 25).


## Origin
It was first payed in front of the Pavillon Henri Chrétien (PHC) of the Observatoire de la Côte d’Azur (OCA) on 01.03.2024.
