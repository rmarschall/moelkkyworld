# Memory Mölkky aka Blind Mölkky

## Rules
This version of Mölkky follows the standard rules except that the Mölkky is set up such that the players cannot see the numbers on the pins. An umpire is required for this version. They will announce the points and then reposition fallen pins. This way the teams will have to remember which pin bares which number.


## Origin
It was first payed in front of the Pavillon Henri Chrétien (PHC) of the Observatoire de la Côte d’Azur (OCA) on 05.03.2024.
