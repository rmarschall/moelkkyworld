# Pairs et Impairs Mölkky

## Rules
This version of Mölkky follows the standard rules except that the score of the numbers is adjusted depending on whether they are even or odd. If one stick falls, the number on the stick is awarded to the team in the following way: If the number is even, these points are added to the current score; if the number is odd, these points are subtracted from the current score. Similarly, if more than one stick falls, the number of sticks determines if points are added or subtracted. An even number of sticks means that a number is added, and an odd number of sticks means that the number is subtracted. The game ends with the first team to reach 25. The game does not end when a team goes over 25. Rather, at that point, the team needs to try to subtract points to reach exactly 25, e.g., at a score of 28, by hitting only the number 3 or three sticks to win.

Altenative end: The team that first reaches either 25 or -25 points wins the game.


## Origin
It was first payed in front of the Pavillon Henri Chrétien (PHC) of the Observatoire de la Côte d’Azur (OCA) on 20.02.2024.
