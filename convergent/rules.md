# Convergent Mölkky

## Rules
This version of Mölkky follows the standard rules except the two teams play from opposite sides of the game, i.e., the game is set up between the two teams.
Alternatively, if there are more than two teams, i.e., n teams, then the game is set up in the middle and the teams play from an equal distance seperated by 360°/n of each other.
Because teams play 'towards' each other the game converges at a central point and the numbers tend to converge back to the centre.


## Origin
It was first payed in front of the Pavillon Henri Chrétien (PHC) of the Observatoire de la Côte d’Azur (OCA) on 19.02.2024.
