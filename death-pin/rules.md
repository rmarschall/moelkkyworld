# Death Pin Mölkky

## Rules
This version of Mölkky follows the standard rules except that the first single pin to be knocked over becomes a 'death pin'. In subsequent throws, if the 'death pin' gets thrown over (even with other pins falling with it), the team falls back to 0 (when the previous score as 25 or less) or to 25 (when the previous score was 26 or more).

Alternative 1: additional 'death pins' can be added.
Alternative 2: the 'death pin' get decided upon randomly before the game starts and is in play from the beginning.


## Origin
It was first payed in front of the Pavillon Henri Chrétien (PHC) of the Observatoire de la Côte d’Azur (OCA) on 21.02.2024.
