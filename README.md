# Mölkky World

Official repository for non-official Mölkky rules!

## Rules

The directories contain the rules and play-example of the different branches of Mölkky World.

- `convergent` a.k.a. post-impact reaccumulation Mölkky
- `extreme` the top-down version of the `stairs` Mölkky
- `stairs` the bottom-up version of the `extreme` Mölkky
- `pairs et impairs` version of Mölkky, a.k.a. `odd and even` Mölkky.
- `death pin` version of Mölkky.
- `wrong-handed option` of Mölkky (not recommended by [WHO])
- `gamblers` version of Mölkky
- `memory` a.k.a. `blind` version of Mölkky
- `wedding bouquet` a.k.a. `back sided` version of Mölkky

## Ideas for future Mölkky
- biathlon Mölkky (relay with one Mölkky per team)
- bowling Mölkky (rolling a ball instead of throwing the stick), also football molkky.
- MegaMölkky (combining both molkky sets)
- MysteryMölkky (a judge scores the game. players must infer the rules of the game based on the score the recieve each round. This can only be played after many other game types have been invented)
- Single Pin Win (must win by scoring only one pin with the exact required point value. In this case, overshooting you score means you just skip your turn)

## Archive

| Date | Type | Comment |
| --- | --- | --- |
| 15.02.2024 | extreme | |
| 16.02.2024 | stairs | 3-miss-to-elimination required |
| 19.02.2024 | convergent | |
| 20.02.2024 | pairs-impairs | |
| 21.02.2024 | death pin | 3 death pins might bee too much |
| 01.03.2024 | wrong-handed |  |
| 04.03.2024 | gamblers |  |
| 05.03.2024 | memory |  |
| 06.03.2024 | wedding bouquet | not really that much fun |
| 07.03.2024 | stairs |  |


